from flask import redirect
import requests
import json
from typing import AnyStr
from random import randint

CLIENT_SECRET = None
CLIENT_ID = None
SCOPES = None
REDIRECT_URI = None

def bad_req_resp():
    return {
        "statusCode": 400
    }

def get_client_secret():
    global CLIENT_SECRET
    if CLIENT_SECRET is not None:
        return CLIENT_SECRET
    with open("/var/openfaas/secrets/client_secret","r") as secret_file:
        data = secret_file.readline()
        print("Loaded Client Secret {}".format(data))
        if data is not None:
            CLIENT_SECRET = data
    return CLIENT_SECRET

def get_client_id():
    global CLIENT_ID
    if CLIENT_ID is not None:
        return CLIENT_ID
    with open("/var/openfaas/secrets/client_id","r") as secret_file:
        data = secret_file.readline()
        print("Loaded Client ID {}".format(data))
        if data is not None:
            CLIENT_ID = data
    return CLIENT_ID

def get_scopes():
    global SCOPES
    if SCOPES is not None:
        return SCOPES
    with open("/var/openfaas/secrets/scopes","r") as secret_file:
        data = secret_file.readline()
        print("Loaded Scopes {}".format(data))
        if data is not None:
            SCOPES = data
    return SCOPES

def get_redirect_uri():
    global REDIRECT_URI
    if REDIRECT_URI is not None:
        return REDIRECT_URI
    with open("/var/openfaas/secrets/redirect_uri","r") as secret_file:
        data = secret_file.readline()
        print("Loaded Redirect Uri {}".format(data))
        if data is not None:
            REDIRECT_URI = data
    return REDIRECT_URI

TOKEN_MAP = {}

def handle(event, context):
    # Receive Spotify redirect with "code" as URI param and "device-id"
    # Make call to Spotify api with:
    #   Code from request
    #   Client ID/ Client Secret from application
    # Return Token + Refresh Token to client

    if event.path.startswith("/device"):
        return handle_client_auth(event)
    elif event.path.startswith("/register"):
        return handle_device_registration(event)
    elif event.path.startswith("/retrieve"):
        return handle_retrieve_token(event)
    elif event.path.startswith("/refresh"):
        return handle_refresh_token(event)
    bad_req_resp()

def handle_retrieve_token(event):
    if not "device" in event.query:
        return bad_req_resp()

    device = event.query["device"]
    if device not in TOKEN_MAP:
        return bad_req_resp()
    
    token_data = TOKEN_MAP[device]
    del TOKEN_MAP[device]

    return {
        "statusCode": 200,
        "body": token_data
    }

def handle_refresh_token(event):

    if not "refresh_token" in event.query:
        return bad_req_resp()

    url = "https://accounts.spotify.com/api/token"

    payload = "grant_type=refresh_token"
    payload += "&redirect_uri=" + get_redirect_uri()
    payload += "&client_secret=" + get_client_secret() 
    payload += "&client_id=" + get_client_id()
    payload += "&refresh_token=" + event.query["refresh_token"]

    headers = {
        'content-type': "application/x-www-form-urlencoded"
        }

    response = requests.request("POST", url, data=payload, headers=headers)

    if response.status_code is not 200:
        return {
            "statusCode": 500
        }

    response_body = json.loads(response.text)
    return {
        "statusCode": 200,
        "body": {
            "access_token": response_body["access_token"]
        }
    }


def generate_device_id(characters: AnyStr):
    device_id = ""

    for i in range(0,3):
        for j in range(0,4):
            device_id += characters[randint(0, len(characters) - 1)]
        device_id += "-"
    
    return device_id[:-1]

def handle_device_registration(event):

    characters = "abcdefghijklmnopqrstuvwxyz0123456789"

    device_id = generate_device_id(characters)


    return {
        "statusCode": 201,
        "body": {
            "device": device_id
        }
    }


def handle_client_auth(event):
    code = None
    if "code" in event.query:
        code = event.query["code"]
    
    device = None
    if "/device/" in event.path:
        device = event.path.replace("/device/", "")
    elif "state" in event.query:
        device = event.query["state"]

    
    if code is None and device is None:
        return bad_req_resp()
    

    # initiate spotify redirect for auth
    if code is None:
        redirect_url = "https://accounts.spotify.com/authorize?response_type=code"
        redirect_url += "&scope=" + get_scopes()
        redirect_url += "&redirect_uri=" + get_redirect_uri()
        redirect_url += "&client_id=" + get_client_id()
        redirect_url += "&state=" + device
        return {
            "statusCode": 302,
            "headers": {
                "Location": redirect_url
            }
        }

    token_data = exchange_code_for_token(code)

    if token_data is None:
        return bad_req_resp()
    
    TOKEN_MAP[device] = token_data

    return {
        "statusCode": 200,
        "body": {
            "code": code,
            "device": device,
            "mesage": "Successfully stored token against device ID"
        }
    }



def exchange_code_for_token(code: AnyStr):
    # submit request to spotify for token + refresh
    token_url = "https://accounts.spotify.com/api/token"

    payload = "grant_type=authorization_code"
    payload += "&redirect_uri=" + get_redirect_uri()
    payload += "&code=" + code
    payload += "&client_secret=" + get_client_secret() 
    payload += "&client_id=" + get_client_id()
    headers = {
        'content-type': "application/x-www-form-urlencoded"
    }

    response = requests.request("POST", token_url, data=payload, headers=headers)

    if response.status_code is not 200:
        return None

    response_body = json.loads(response.text)
    return response_body