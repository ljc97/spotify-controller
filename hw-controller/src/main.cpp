#include <Arduino.h>
#include <string.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

const int PLAY_PAUSE_PIN = 0;
unsigned long PLAY_PAUSE_LASTACT = 0;

const int BACK_PIN = 0;
unsigned long BACK_LASTACT = 0;

const int FORWARD_PIN = 0;
unsigned long FORWARD_LASTACT = 0;

const int VOL_PIN = 0;
unsigned long VOL_LASTACT = 0; // possibly not used
int VOL_LASTVAL = 0;

unsigned long MIN_DELAY = 50;

String TOKEN = "";
unsigned long TOKEN_EXPIRY = 0;
String REFRESH = "";

String REGISTER_DEVICE_URL = "http://gateway.openfaas.10.8.0.21.xip.io/function/spotify-token/register";
String RETRIEVE_TOKEN_URL = "http://gateway.openfaas.10.8.0.21.xip.io/function/spotify-token/retrieve?device=";
String REFRESH_TOKEN_URL = "http://gateway.openfaas.10.8.0.21.xip.io/function/spotify-token/refresh?refresh_token=";


void set_volume(int volume)
{
  Serial.println("Spotify->Volume=" + volume);
}

void playpause()
{
  Serial.println("Spotify->PlayPause");
}

void next_track()
{
  Serial.println("Spotify->Next");
}

void prev_track()
{
  Serial.println("Spotify->Previous");
}

String retrieve_device_id()
{
  HTTPClient http;
  http.begin(REGISTER_DEVICE_URL.c_str());
  int httpResponseCode = http.GET();
  if (httpResponseCode == 201) {
    String payload = http.getString();
    StaticJsonDocument<500> data;
    DeserializationError err = deserializeJson(data, payload); // check err?
    String device_id = data["device"];
    http.end();
    return device_id;
  }
  // Free resources
  http.end();
  return "";
}

boolean retrieve_device_registration(String device_id)
{
  boolean status = false;
  HTTPClient http;
  http.begin(RETRIEVE_TOKEN_URL + device_id);
  int httpResponseCode = http.GET();
  if (httpResponseCode == 200) {
    String payload = http.getString();
    StaticJsonDocument<500> data;
    DeserializationError err = deserializeJson(data, payload); // check err?
    
    String token = data["access_token"];
    String refresh = data["refresh_token"];
    TOKEN = token;
    REFRESH = refresh;
    TOKEN_EXPIRY = millis() + 3590000; // 3600s expiry
    
    status = true;
  }
  // Free resources
  http.end();
  return status;
}



bool is_token_expired()
{
  // check if token is expired
  return millis() >= TOKEN_EXPIRY; // overflow of timer before reset?
}

bool refresh_token()
{
  // make refresh call to API, store token 
  //    can't send directly to spotify as need client_id/client_secret
  HTTPClient http;
  http.begin(REFRESH_TOKEN_URL + REFRESH);
  int httpResponseCode = http.GET();
  if (httpResponseCode == 200) {
    String payload = http.getString();
    StaticJsonDocument<500> data;
    DeserializationError err = deserializeJson(data, payload); // check err?

    String token = data["access_token"];
    TOKEN = token;
    TOKEN_EXPIRY = millis() + 3590000; // 3600s expiry

  }
  // Free resources
  http.end();
  return false;
}

void call_spotify(String endpoint)
{
  if (is_token_expired())
  {
    Serial.println("Token Expired, Refreshing");
    bool result = refresh_token();
    if (!result)
    {
      Serial.println("Refresh Failed!");
      // expired, failed to refresh, return
      return;
    }
    Serial.println("Token Successfully Refreshed");
  }

}

void loop()
{
  // read each button, handle any required actions
  if (digitalRead(PLAY_PAUSE_PIN) && millis() > PLAY_PAUSE_LASTACT + 250)
  {
    PLAY_PAUSE_LASTACT = millis();
    playpause();
  }

  if (digitalRead(BACK_PIN) && millis() > BACK_LASTACT + 250)
  {
    BACK_LASTACT = millis();
    prev_track();
  }

  if (digitalRead(FORWARD_PIN) && millis() > VOL_LASTACT + 250)
  {
    FORWARD_LASTACT = millis();
    next_track();
  }

  int volume = analogRead(VOL_PIN) / 1023;
  if (abs(volume - VOL_LASTVAL) > 1)
  {
    VOL_LASTACT = millis();
    VOL_LASTVAL = volume;
        set_volume(volume);
  }
}

void setup()
{
  Serial.begin(9600);

  // setup digital i/o on play/back/forward
  pinMode(PLAY_PAUSE_PIN, INPUT);
  PLAY_PAUSE_LASTACT = millis();

  pinMode(BACK_PIN, INPUT);
  BACK_LASTACT = millis();

  pinMode(FORWARD_PIN, INPUT);
  FORWARD_LASTACT = millis();

  // setup analog i/o on vol
  pinMode(VOL_PIN, INPUT);
  VOL_LASTACT = millis();
  VOL_LASTVAL = analogRead(VOL_PIN) / 1023;


  Serial.println("Retrieve Device ID");
  // retrieve device_id
  String device_id = retrieve_device_id();
  if(device_id == "") {
    exit(0);
  }

  Serial.println("Got Device ID " + device_id);

  Serial.println("Getting Device Registration Info");
  // call through to device registration service to see if token is available
  boolean device_registration_status = retrieve_device_registration(device_id);

  Serial.println("Device Reg Status = " + device_registration_status);
  while (device_registration_status == false)
  {
    delay(5000);
   
    device_registration_status = retrieve_device_registration(device_id);
    Serial.println("Device Reg Status = " + device_registration_status);
  }


}